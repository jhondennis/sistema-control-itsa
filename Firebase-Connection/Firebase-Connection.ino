#include <ESP8266WiFi.h>
#include "FirebaseESP8266.h"

// Mod temperature
#include <OneWire.h>
#include <DallasTemperature.h>

// Credenciales wifi
const char *ssid = ""; // aqui va el nombre de la red
const char *password = ""; // contraseña del wifi

// Credenciales Proyecto Firebase
const char *FIREBASE_HOST = "";  // end point a la base de datos de firebase
const char *FIREBASE_AUTH = "";  // llave de autenticacion

// Firebase Data object in the global scope
FirebaseData firebaseData;

// Define conexion and init values
#define ONE_WIRE_BUS 12
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
float Celsius = 0;

#define D4 2
#define D3 4

void setup()
{
  delay(1000);
  sensors.begin();   //Se inicia el sensor
  Serial.begin(115200);
  pinMode(D4,OUTPUT);
  pinMode(D3,OUTPUT);
  Serial.println();

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(250);
  }
  Serial.print("\nConectado al Wi-Fi");
  Serial.println();

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);
  Firebase.setInt(firebaseData, "S1", 0);
  Firebase.setInt(firebaseData, "S2", 0);
}

void loop()
{
  sensors.requestTemperatures();   //Se envía el comando para leer la temperatura
  Celsius = sensors.getTempCByIndex(0);
  Serial.print(Celsius);
  Serial.print(" C  ");
  Serial.println("");
  Firebase.setFloat(firebaseData, "TEMP", Celsius);
  delay(250);

  // String nodo = "itsa";
  Firebase.getInt(firebaseData, "S1");
  Serial.println(firebaseData.intData());
  delay(250);

  if(1 == firebaseData.intData()) {
    digitalWrite(D4,LOW);
    Serial.println("Luz ON");
  } else if(firebaseData.intData()== 0) {
    digitalWrite(D4,HIGH);
    Serial.println("Luz OFF");
  }

  Firebase.getInt(firebaseData, "S2");
  Serial.println(firebaseData.intData());
  delay(250);
  if(1 == firebaseData.intData()) {
    digitalWrite(D3,HIGH);
    Serial.println("Luz ON");
  } else if(firebaseData.intData()== 0) {
    digitalWrite(D3,LOW);
    Serial.println("Luz OFF");
  }

} // End Loop
