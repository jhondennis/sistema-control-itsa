# DOCKER - NODE RED

```
FROM nodered/node-red
MAINTAINER dennis <dennis.laurel.v@gmail.com>
RUN npm install node-red-contrib-flightaware
RUN npm install node-red-contrib-firebase
RUN npm install node-red-node-arduino
RUN npm install node-red-node-random
RUN npm install node-red-node-twilio
RUN npm install node-red-contrib-ibm-watson-iot
RUN npm install node-red-node-serialport
RUN npm install node-red-node-email
RUN npm install node-red-contrib-telegrambot
RUN npm install node-red-contrib-splitter
RUN npm install node-red-dashboar
```

Creamos el archivo [docker-compose.yaml] con el siguiente contenido

```
version: "2"
	services:
		nodered:
			image: dennislaurel/node-red-firebase:0.2
		ports:
			- 1880:1880
		volumes:
			- /usr/share/zoneinfo/America/La_Paz:/etc/localtime
		- /[route-file]:/data
		restart: unless-stopped
```

Ejecutar el archivo:

```
$ sudo docker-compose up -d // la bandera -d es para ejecución underground
$ sudo docker-compose down // Detener el contenedor
```

## BIBLIOGRAFIA

- [https://www.zeitverschiebung.net/es/timezone/america--la_paz](https://www.zeitverschiebung.net/es/timezone/america--la_paz)
  -- Time zone
- [https://medium.com/@eboriolinarez/actualizar-nodejs-a-una-versi%C3%B3n-superior-o-inferior-utilizando-npm-1a3c4ac194dd](https://medium.com/@eboriolinarez/actualizar-nodejs-a-una-versi%C3%B3n-superior-o-inferior-utilizando-npm-1a3c4ac194dd)
  -- Install version previous
- [https://nodered.org/docs/getting-started/docker#container-shell](https://nodered.org/docs/getting-started/docker#container-shell)
  -- Manual node-red running under docker
- [https://aprendiendoarduino.wordpress.com/2020/03/08/instalacion-node-red/](https://aprendiendoarduino.wordpress.com/2020/03/08/instalacion-node-red/)
  -- Node-red raspberry
- [https://www.dataplicity.com/](https://www.dataplicity.com/)
  -- Remoute control your RPI our Ubuntu
